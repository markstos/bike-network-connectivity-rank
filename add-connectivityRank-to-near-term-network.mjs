import fs from 'fs';
import { featureEach } from '@turf/meta';
import { center } from '@turf/center'
import { booleanWithin } from "@turf/boolean-within";

// Use version 6.5 because 7.0 has a bug
import booleanIntersects from '@turf/boolean-intersects';

const nearTermSegmentsFC = JSON.parse(fs.readFileSync('2024-06-03-near-term-low-stress-SINGLEPART.geojson'));
const hexFC = JSON.parse(fs.readFileSync('2025-05-25-minneapolis-hex-grid.geojson'));
const isochrones = {
  one: JSON.parse(fs.readFileSync('1-minute-isochrone.geojson')),
  two: JSON.parse(fs.readFileSync('2-minute-isochrone.geojson')),
  three: JSON.parse(fs.readFileSync('3-minute-isochrone.geojson')),
  four: JSON.parse(fs.readFileSync('4-minute-isochrone.geojson')),
  five: JSON.parse(fs.readFileSync('5-minute-isochrone.geojson')),
};

const segmentsTotal = nearTermSegmentsFC.features.length;
let i = 1;

featureEach(nearTermSegmentsFC, segment => {
  segment.properties.connectivityRank = 0;

  console.warn(`processing ${i}/${segmentsTotal} segments`)
  i++;

  featureEach(hexFC, hex => {
    if (booleanIntersects(hex, segment)) {
      const hexCenter = center(hex);
      
      // Even more precise would taking the center of the segment rather than the hex
      // Requirng to first split the the segment using the hex and using the segment
      // whose center within the hex-- there should be only one.
      if (booleanWithin(hexCenter, isochrones.one)) {
        segment.properties.connectivityRank += 1;
      }
      else if (booleanWithin(hexCenter, isochrones.two)) {
        segment.properties.connectivityRank += 2;
      }
      else if (booleanWithin(hexCenter, isochrones.three)) {
        segment.properties.connectivityRank += 3;
      }
      else if (booleanWithin(hexCenter, isochrones.four)) {
        segment.properties.connectivityRank += 4;
      }
      else if (booleanWithin(hexCenter, isochrones.five)) {
        segment.properties.connectivityRank += 5;
      }
    }
  })
})

nearTermSegmentsFC.properties = {
  dateGenerated: new Date(),
  author: 'Mark Stosberg <mark@stosberg.com>',
};

fs.writeFileSync('near-term-with-connectivity-rank.geojson', JSON.stringify(nearTermSegmentsFC));
