export class IsoChroneError extends Error {
	constructor(response, ...args) {
		super(`Isochrone error. No coverage or not near a road: ${response.status} ${response.statusText}`, ...args);
		this.response = response;
	}
}

