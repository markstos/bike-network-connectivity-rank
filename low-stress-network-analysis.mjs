import fs from 'fs';
import { featureEach } from '@turf/meta';
// Turf 7 uses this import style
//import { booleanIntersects } from '@turf/boolean-intersects';
import booleanIntersects from '@turf/boolean-intersects';
import { center } from '@turf/center'
import { IsoChroneError } from './lib/errors.mjs';
import Promise from 'bluebird';
import { union } from 'polyclip-ts';
import { multiPolygon } from '@turf/helpers';
import { getCoords } from '@turf/invariant';
import { booleanWithin } from "@turf/boolean-within";
import { lineSplit } from '@turf/line-split';


const inFile = fs.readFileSync('2025-05-25-minneapolis-hex-grid.geojson');
const hexFC = JSON.parse(inFile);

const existingNetworkFile = fs.readFileSync('2024-06-03-existing-low-stress-SINGLEPART.geojson');
const existingNetworkSegments = JSON.parse(existingNetworkFile);

// How many concurrent requests can your isochrone server handle?
const CONCURRENCY = Number(process.env.ISOCHRONE_CONCURRENCY) || 16;

// a Valhalla instance which has loaded an .osm.pbf file of the current and planned bike networks only.
process.env.ISOCHRONE_URL='http://localhost:8002/isochrone?json={"polygons":true,"locations":[{"lat":{{lat}}, "lon":{{lon}}}],"costing":"bicycle","contours":[{"time":{{time}}}]}'

const totalHexes = hexFC.features.length;
const totalSegments = existingNetworkSegments.features.length;

let i = 0;
let matchCnt = 0;

import util from 'util';

const hexsWithExistingSegments = hexFC.features
  .filter(hexCell => {
     i++;
     let matched = false;
     existingNetworkSegments.features.forEach(segment => {
       if (matched) {
         return;
       }
       if (booleanIntersects(hexCell, segment)) {
         matched = true;
       }
     })

     if (matched) {
       matchCnt = matchCnt+1;
     }
     console.warn(`Filtering ${i}/${totalHexes} against ${totalSegments} segments, matchCnt ${matchCnt}`)
     return matched;
   })
  //.map(center)

fs.writeFileSync('hexes-with-existing-segments.geojson', JSON.stringify(hexsWithExistingSegments));

// Minutes for isochrone generation
const times = [1,2,3,4,5];

await Promise.map(
  times,
  async (time) => {
     const multiPolygon = await getMultiPolygon(hexsWithExistingSegments,time)
     fs.writeFileSync(`${time}-minute-isochrone.geojson`, JSON.stringify(multiPolygon));
  }
)

process.exit();

// ---------------------------------------------------------------------------


// Expects ISOCHRONE_URL to contain {{lat}} and {{lon}} and {{time}}
function getIsochroneURL(lat, lon, time) {
  return process.env.ISOCHRONE_URL.replace(/\{\{([^}]+)}\}/g, (_match, key) => {
    if (key === 'lat') {
      return lat;
    }
    if (key === 'lon') {
      return lon;
    }
    if (key === 'time') {
      return time;
    }
    throw new Error(`Invalid placeholder: ${key}`);
  });
}

// get a lon, lat and time, return GeoJSON Feature <MultiPolygon> representing isochrone for that point.
// @throws {IsoChroneError} if there's not coverage or point is not near a road.
async function getIsoChrone(lon, lat, time) 

// `time` arg goes first so it can be bound
async function isoChroneForOneFeature(time, feature) {
  const [lon, lat] = getCoords(center(feature));
  return Promise.resolve(getIsoChrone(lon, lat, time))
    .catch(IsoChroneError, (err) => {
      console.warn('\t❓ No isochrone found', err.message, [lon, lat]);
      return undefined;
    });
}

// given array of GeoJSON features return a multiPolygon feature
async function getMultiPolygon(features, time) {

  // Each file contains a GeoJSON FeatureCollection with one or more features.
  // If there's more than one Feature, we update the the properties for all.
  const multiPolygonFeatures = await Promise.map(
    features,
    isoChroneForOneFeature.bind(null, time),
    { concurrency: CONCURRENCY },
  );

  let i = 0;
  const total = multiPolygonFeatures.length;

  // Filter and union all the isochrones on the layer together and return;
  return multiPolygon(multiPolygonFeatures
    .filter(Boolean)
    .reduce((acc, feature) => {
      // Progress report for large sets
      i += 1;
      if (i % 100 === 0) {
        console.warn(`\t🔄 merged ${i}/${total} polygons`);
      }
      return union(getCoords(acc), getCoords(feature));
    }));
}
